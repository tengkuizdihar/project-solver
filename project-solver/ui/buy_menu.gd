extends Control

const WEAPON_BUY_MENU = {
	"pm9": {
		"weapon_id": "pm9",
		"weapon_name": "Pistol Machine 9",
		"weapon_description": """Pistol Machine 9 or PM9
Made in The Kingdom, now called The Republic
Perfect when you don't have money
Need care when pulling the trigger
May hurt someone at the end of the barrel""",
	},
	"rail_gun": {
		"weapon_id": "rail_gun",
		"weapon_name": "Rail Gun",
		"weapon_description": """Rail Gun
Zoom unto your target's head and pull the trigger
Simple as""",
	},
	"rf7": {
		"weapon_id": "rf7",
		"weapon_name": "Rifle Force 7",
		"weapon_description": """Rifle Force 7 or RF7
Made in Milium, hasn't changed their name since 300 years ago
Specially designed to function on every terrain and weather condition known to man
Can still shoot underwater
Certified to be assembled and used by a human older than 5 years old""",
	},
	"frag_grenade": {
		"weapon_id": "frag_grenade",
		"weapon_name": "Frag Grenade",
		"weapon_description": """It's a frag grenade, what more do you want?""",
	},
	"flash_grenade": {
		"weapon_id": "flash_grenade",
		"weapon_name": "Flash Grenade",
		"weapon_description": """Think fast Chucklenuts!""",
	},
	"smoke_grenade": {
		"weapon_id": "smoke_grenade",
		"weapon_name": "Smoke Grenade",
		"weapon_description": """You can shoot through it?!""",
	},
}

@export var weapon_button: PackedScene = null
@onready var weapon_buy_button_container = $HBoxContainer/MarginContainer/VBoxContainer
@onready var weapon_description_label = $HBoxContainer/CenterContainer/WeaponDescriptionLabel

# Called when the node enters the scene tree for the first time.
func _ready():
	Util.handle_err(State.connect("state_player_buy_menu_open", Callable(self, "_on_state_player_buy_menu_open")))
	_on_state_player_buy_menu_open(State.get_state("player_buy_menu_open"))

	# clear text before proceeding
	weapon_description_label.text = ""

	# remove preview button
	for c in weapon_buy_button_container.get_children():
		c.queue_free()

	# create buttons for buying
	var weapon_buy_data_keys = WEAPON_BUY_MENU.keys()
	weapon_buy_data_keys.sort()

	for weapon_key in WEAPON_BUY_MENU.keys():
		# instantiate and add to child
		var instanced = weapon_button.instantiate()
		weapon_buy_button_container.add_child(instanced)

		# set data
		var weapon_data = WEAPON_BUY_MENU[weapon_key]
		instanced.text = weapon_data.weapon_name

		# set signals
		instanced.pressed.connect(self.try_to_buy_weapon.bind(weapon_key))
		instanced.mouse_entered.connect(self.button_on_hover.bind(weapon_key))


func button_on_hover(weapon_id) -> void:
	weapon_description_label.text = WEAPON_BUY_MENU[weapon_id]["weapon_description"]


func try_to_buy_weapon(weapon_id) -> void:
	GameMode.gmd_buy_weapon.rpc(weapon_id)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _on_state_player_buy_menu_open(is_open):
	if is_open:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		self.show()

	if not is_open:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		self.hide()
