extends Control


@export var player_row_scene: PackedScene
@onready var team_a_player_list = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/TeamAContainer/PlayerListContainer/PlayerList
@onready var team_b_player_list = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/TeamBContainer/PlayerListContainer/PlayerList
@onready var team_unassigned_player_list = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/TeamUnassignedContainer/PlayerListContainer/PlayerList
@onready var team_unassigned_section = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/TeamUnassignedContainer

# peer_id => reference to row node
var player_info_row_path = {}

func _ready():
	sync_from_lobby_info(State.get_state("multiplayer_lobby_player_info"))
	State.connect("state_multiplayer_lobby_player_info", self.sync_from_lobby_info)


func clear_preview_player_list_for_editor() -> void: 
	var player_lists = [
		team_a_player_list,
		team_b_player_list,
		team_unassigned_player_list,
	]

	for list_info in player_lists:
		for player_row in list_info.get_children():
			player_row.free()



func sync_from_lobby_info(lobby_info: Dictionary) -> void:
	clear_preview_player_list_for_editor() # FIXME: yes, we clear the entire board everytime someone kill, die, comes in, etc.

	for peer_id in lobby_info.keys():
		var player_info = lobby_info[peer_id]
		var team_player_list = team_unassigned_player_list
		match player_info.team_enum:
			Global.TEAM.TEAM_1:
				team_player_list = team_a_player_list
			Global.TEAM.TEAM_2:
				team_player_list = team_b_player_list

		if player_info:
			var player_row = player_row_scene.instantiate()
			team_player_list.add_child(player_row)
			player_row.ready.connect(player_row.set_texts.bind(Lobby.is_peer_id_me(peer_id), peer_id, player_info.nametag, 0, 0, 0, 0, 0))
			player_info_row_path[peer_id] = player_row
			player_row.set_texts(Lobby.is_peer_id_me(peer_id), peer_id, player_info.nametag, player_info.kill, player_info.death, player_info.assist, 0, 0)

	if team_unassigned_player_list.get_children().size() == 0:
		team_unassigned_section.hide()
