extends PanelContainer


var keybind_setting_item_packed = preload("res://ui/keybind_setting_item.tscn")
var rst_value_row_packed = preload("res://ui/training_history_rst_value_row.tscn")

@onready var bullet_decal_max_spinbox = $VBoxContainer/TabContainer/Game/MarginContainer/ScrollContainer/VBoxContainer/BulletDecalMax/SpinBox
@onready var mouse_speed_spinbox = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/MouseSpeed/SpinBox
@onready var master_volume_hslider = $VBoxContainer/TabContainer/Audio/ScrollContainer/MarginContainer/VBoxContainer/MasterVolume/HSlider
@onready var gameplay_volume_hslider = $VBoxContainer/TabContainer/Audio/ScrollContainer/MarginContainer/VBoxContainer/GameplayVolume/HSlider
@onready var music_volume_hslider = $VBoxContainer/TabContainer/Audio/ScrollContainer/MarginContainer/VBoxContainer/MusicVolume/HSlider
@onready var player_control_container = $VBoxContainer/TabContainer/Controls/ScrollContainer/MarginContainer/VBoxContainer

@onready var crosshair_color_picker = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/CrosshairColor/ColorPicker
@onready var crosshair_center_radius_slider = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/CrosshairCenterRadius/Slider
@onready var crosshair_width_slider = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/CrosshairWidth/Slider
@onready var crosshair_length_slider = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/CrosshairLength/Slider
@onready var crosshair_spacing_slider = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/CrosshairSpacing/Slider
@onready var crosshair_center_dot_checkbox = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/CrosshairCenterDot/CheckBox
@onready var crosshair_leg_checkbox = $VBoxContainer/TabContainer/Player/MarginContainer/ScrollContainer/VBoxContainer/CrosshairLeg/CheckBox

@onready var training_tab = $VBoxContainer/TabContainer/Training
@onready var rst_history_table_values = $VBoxContainer/TabContainer/Training/MarginContainer/TabContainer/History/MarginContainer/ScrollContainer/HistoryTable/TableValues
@onready var rst_highest_target_count_label = $VBoxContainer/TabContainer/Training/MarginContainer/TabContainer/Record/MarginContainer/ScrollContainer/VBoxContainer/HBoxContainer/HighestTargetCountValue
@onready var rst_fastest_time_label = $VBoxContainer/TabContainer/Training/MarginContainer/TabContainer/Record/MarginContainer/ScrollContainer/VBoxContainer/HBoxContainer_2/HighestTargetPerSecondValues

@onready var rst_setting_bpm_spinbox = self.get_node("%BeatsPerMinuteSpinBox")
@onready var rst_max_hit_before_timer_spinbox = self.get_node("%TargetCountBeforeRestartSpinBox")

@onready var god_mode_checkbox = $VBoxContainer/TabContainer/Game/MarginContainer/ScrollContainer/VBoxContainer/GodMode/CheckBox

func _ready():
	_apply_from_config(Config.state)
	_apply_from_history(Score.training_history)
	Util.handle_err(Config.connect("config_changed",Callable(self,"_apply_from_config")))
	Util.handle_err(Score.connect("training_history_updated",Callable(self,"_apply_from_history")))

	# Training Tab will only be hidden when player is in a level and mode is not aim training
	if get_tree().get_nodes_in_group(Global.GROUP_LEVEL).size() > 0 and State.get_state("game_mode") != Global.GAME_MODE.AIM_TRAINING:
		training_tab.queue_free()
		Score.disconnect("training_history_updated",Callable(self,"_apply_from_history"))


func _apply_from_config(config_state: Dictionary):
	bullet_decal_max_spinbox.value = config_state.game.bullet_decal_max
	god_mode_checkbox.set_pressed(config_state.game.god_mode_enabled)
	
	mouse_speed_spinbox.value = config_state.player.mouse_speed

	master_volume_hslider.value = config_state.audio.master_volume
	gameplay_volume_hslider.value = config_state.audio.gameplay_volume
	music_volume_hslider.value = config_state.audio.music_volume
	
	_init_keybinds_options(config_state)
	_init_crosshair_configs(config_state)


func apply_to_config():
	Config.change_config("game", "bullet_decal_max", bullet_decal_max_spinbox.value)
	Config.change_config("game", "god_mode_enabled", god_mode_checkbox.is_pressed())
	if(god_mode_checkbox.is_pressed()):
		_set_god_mode()
	
	Config.change_config("player", "mouse_speed", mouse_speed_spinbox.value)

	Config.change_config("audio", "master_volume", master_volume_hslider.value)
	Config.change_config("audio", "gameplay_volume", gameplay_volume_hslider.value)
	Config.change_config("audio", "music_volume", music_volume_hslider.value)

	for c in player_control_container.get_children():
		Config.change_config("keyboard_control", c.input_event_name, c.scancode)

	_set_crosshair_configs()

	Config.apply_config()
	Config.emit_config_changed()

	# Training Config
	Score.rst_metronome_change_bpm(rst_setting_bpm_spinbox.value)
	Score.rst_change_max_hit(rst_max_hit_before_timer_spinbox.value)
	Score.emit_history_changes()

func _set_god_mode():
	DisplayServer.window_set_max_size(Vector2i(640,480))

func _init_keybinds_options(config_state: Dictionary):
	for c in player_control_container.get_children():
		c.queue_free()

	for k in config_state.keyboard_control.keys():
		var found_scancode = config_state.keyboard_control[k]
		_add_keybind_settings_item(k, found_scancode)


func _init_crosshair_configs(config_state: Dictionary):
	crosshair_color_picker.color = config_state.player.crosshair_color_0
	crosshair_center_radius_slider.value = config_state.player.crosshair_center_radius
	crosshair_width_slider.value = config_state.player.crosshair_width
	crosshair_length_slider.value = config_state.player.crosshair_len
	crosshair_spacing_slider.value = config_state.player.crosshair_spacing
	crosshair_center_dot_checkbox.set_pressed(config_state.player.crosshair_center_enabled)
	crosshair_leg_checkbox.set_pressed(config_state.player.crosshair_legs_enabled)


func _set_crosshair_configs():
	Config.change_config("player", "crosshair_color_0", crosshair_color_picker.color)
	Config.change_config("player", "crosshair_center_radius", crosshair_center_radius_slider.value)
	Config.change_config("player", "crosshair_width", crosshair_width_slider.value)
	Config.change_config("player", "crosshair_len", crosshair_length_slider.value)
	Config.change_config("player", "crosshair_spacing", crosshair_spacing_slider.value)
	Config.change_config("player", "crosshair_center_enabled", crosshair_center_dot_checkbox.is_pressed())
	Config.change_config("player", "crosshair_legs_enabled", crosshair_leg_checkbox.is_pressed())


func _add_keybind_settings_item(input_event_name: String, scancode: int) -> void:
	var keybind_setting_item = keybind_setting_item_packed.instantiate()
	keybind_setting_item.input_event_name = input_event_name
	keybind_setting_item.scancode = scancode

	player_control_container.add_child(keybind_setting_item)


func _on_ApplyAndSaveButton_pressed():
	apply_to_config()


func _on_ResetButton_pressed():
	Config.reset_config()

func _apply_from_history(history: Dictionary) -> void:
	rst_setting_bpm_spinbox.value = history[Score.RST_CURRENT_METRONOME_BPM]
	rst_max_hit_before_timer_spinbox.value = history[Score.RST_MAX_HIT_BEFORE_TIMER]

	for c in rst_history_table_values.get_children():
		c.queue_free()

	var highest_target_count = 0.0

	var highest_target_per_second = 0.0
	var highest_target_per_second_target_count = 0.0

	var rst_history = history[Score.RST_HISTORY_KEY_V1]
	for i in range(rst_history.size()):
		var h = rst_history[i]

		# =============== Handle Records ===============
		var target_per_second = h.target_count / h.completion_time_second
		if target_per_second > highest_target_per_second:
			highest_target_per_second = target_per_second
			highest_target_per_second_target_count = h.target_count

		if h.target_count > highest_target_count:
			highest_target_count = h.target_count

		# =============== Handle History ===============
		var new_value = rst_value_row_packed.instantiate()
		rst_history_table_values.add_child(new_value)

		var minute = floor(h.completion_time_second / 60)
		var completion_time_formatted = "%02d:%02.3f" % [minute, fmod(h.completion_time_second, 60.0)]

		var accuracy_percentage_formatted = ("%d" % (h.accuracy * 100)) + "%"

		var target_count_formatted = ("%d" % h.target_count) + " Targets"

		new_value.set_row_values("%d"% (i + 1), h.create_datetime, completion_time_formatted, accuracy_percentage_formatted, target_count_formatted)

	rst_highest_target_count_label.text = "%d Target(s)" % [highest_target_count]
	rst_fastest_time_label.text = "%.3f target(s) per second for %d targets" % [highest_target_per_second, highest_target_per_second_target_count]
