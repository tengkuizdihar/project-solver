extends PanelContainer

@onready var player_info_row_scene = load("res://ui/multiplayer/player_info_row.tscn")
@onready var player_info_container = self.get_node("%PlayerInfoContainer")
@onready var start_game_button = self.get_node("%StartGameButton")

func _ready():
	State.connect("state_multiplayer_lobby_player_info", _on_lobby_player_info)
	self._on_lobby_player_info(State.get_state("multiplayer_lobby_player_info"))

func _on_leave_lobby_button_pressed():
	Lobby.leave_lobby()

func _on_lobby_player_info(value: Dictionary) -> void:
	start_game_button.disabled = not Lobby.is_lobby_master()

	for c in self.player_info_container.get_children():
		c.queue_free()

	var keys = value.keys()
	keys.sort()

	for k in keys:
		# TODO: in the future, change this into something like "is_lobby_admin" or something
		var kick_disable = not Lobby.is_lobby_master() or multiplayer.get_unique_id() == k

		var new_row = player_info_row_scene.instantiate()
		
		self.player_info_container.add_child(new_row)

		if not new_row.is_node_ready():
			await new_row.ready

		new_row.set_player_info(value[k].nametag, k, kick_disable)

func _on_start_game_button_pressed():
	var new_state = GameMode.gmd_initial_state_builder()
	GameMode.gmd_change_state.rpc(new_state, State.get_state("multiplayer_lobby_info")["level_path"])

