extends CenterContainer


func _on_create_button_pressed():
	var nametag_value = self.get_node("%NameTagEdit").get_text()
	var password_value = self.get_node("%PasswordEdit").get_text()
	var port_value = self.get_node("%PortEdit").get_text()

	Lobby.create_server_and_enter_lobby(nametag_value, password_value, port_value.to_int())
