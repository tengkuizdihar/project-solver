extends ColorRect


func _ready():
	Util.handle_err(Config.connect("config_changed",Callable(self,"set_crosshair_from_config")))
	set_crosshair_from_config(Config.state)


func set_crosshair_from_config(config: Dictionary) -> void:
	material.set_shader_parameter("center_enabled", config.player.crosshair_center_enabled)
	material.set_shader_parameter("legs_enabled", config.player.crosshair_legs_enabled)
	material.set_shader_parameter("inverted", config.player.crosshair_inverted)
	material.set_shader_parameter("color_0", config.player.crosshair_color_0)
	material.set_shader_parameter("center_radius", config.player.crosshair_center_radius)
	material.set_shader_parameter("width", config.player.crosshair_width)
	material.set_shader_parameter("len", config.player.crosshair_len)
	material.set_shader_parameter("spacing", config.player.crosshair_spacing)
	material.set_shader_parameter("spread", config.player.crosshair_spread)
	material.set_shader_parameter("leg_alpha", config.player.crosshair_leg_alpha)
	material.set_shader_parameter("top_leg_alpha", config.player.crosshair_top_leg_alpha)
