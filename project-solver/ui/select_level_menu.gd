extends PanelContainer

@export var level_item_scene: PackedScene
@export_dir var level_directory = "res://world_item/levels"

@onready var vbox_container = self.get_node("%VBoxContainer")


func _ready():
	# Generate the items for the select menu
	# Filled with option for levels
	for n in Util.directory_file_names(level_directory, "tscn"):
		if "training" in n: # skip displaying level if it has "training" in its name
			continue

		var level_path = level_directory + "/" + n

		var level_item = level_item_scene.instantiate()
		vbox_container.add_child(level_item)

		level_item.level_path = level_path
		level_item.item_text = n
