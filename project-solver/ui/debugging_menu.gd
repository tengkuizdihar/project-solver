extends ScrollContainer


func _on_network_delay_randomizer_spin_box_value_changed(value:float):
	State.set_state("multiplayer_delay", value / 1000.0)


func _on_network_delay_spin_box_value_changed(value:float):
	State.set_state("multiplayer_delay_random", value / 1000.0)
