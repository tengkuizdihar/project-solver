extends Control

@export_file("*.tscn") var level_scene_path
@export_file("*.tscn") var tutorial_scene_path
@export_file("*.tscn") var training_scene_path
@export_file("*.tscn") var training_programmed_scene_path

@onready var start_programmed_training = self.get_node("%StartProgrammedTraining")
@onready var menu_container = $ColumnContainer/MarginContainer2
@onready var version_label = $ColumnContainer/MarginContainer2/Logo/VBoxContainer/VersionLabel

@onready var about_menu = self.get_node("%AboutMenu")
@onready var option_menu = self.get_node("%OptionMenu")
@onready var select_level_menu = self.get_node("%SelectLevelMenu")
@onready var multiplayer_menu = self.get_node("%MultiplayerMenu")

func _ready():
	# Focus the player's cursor to the start training button
	start_programmed_training.grab_focus()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	# Hide every single menu
	hide_all_menu()

	version_label.text = Global.GAME_VERSION


func hide_all_menu():
	for i in menu_container.get_children():
		if not ("Logo" in i.get_name()):
			i.hide()

func _on_SelectLevel_pressed():
	var before_hide_visibility = select_level_menu.visible
	hide_all_menu()
	select_level_menu.visible = !before_hide_visibility


func _on_Option_pressed():
	var before_hide_visibility = option_menu.visible
	hide_all_menu()
	option_menu.visible = !before_hide_visibility


func _on_Exit_pressed():
	get_tree().quit()


func _on_About_pressed():
	var before_hide_visibility = about_menu.visible
	hide_all_menu()
	about_menu.visible = !before_hide_visibility


func _on_StartTraining_pressed():
	Util.change_level(training_scene_path, Global.GAME_MODE.AIM_TRAINING)

func _on_StartProgrammedTraining_pressed():
	Util.change_level(training_programmed_scene_path, Global.GAME_MODE.AIM_TRAINING)


func _on_multiplayer_pressed():
	var before_hide_visibility = multiplayer_menu.visible
	hide_all_menu()
	multiplayer_menu.visible = !before_hide_visibility
