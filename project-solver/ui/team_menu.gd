extends Control


func handle_visibility(new_is_visible: bool):
	if new_is_visible:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		self.show()
		$Sides/Team1/PanelContainer/Button.grab_focus()
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		self.hide()


func _on_button_Team1_pressed():
	GameMode.gmd_choose_team.rpc(Global.TEAM.TEAM_1)


func _on_button_Team2_pressed():
	GameMode.gmd_choose_team.rpc(Global.TEAM.TEAM_2)
