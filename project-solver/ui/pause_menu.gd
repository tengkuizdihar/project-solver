extends Control

@export_file("*.tscn") var main_menu_scene_path

@onready var menu_container = $HBoxContainer/MarginContainer2
@onready var option_menu = $HBoxContainer/MarginContainer2/OptionMenu
@onready var about_menu = $HBoxContainer/MarginContainer2/AboutMenu
@onready var multiplayer_menu = $HBoxContainer/MarginContainer2/MultiplayerMenu

# Called when the node enters the scene tree for the first time.
func _ready():
	Util.handle_err(State.connect("state_player_paused", Callable(self, "_on_state_player_paused")))
	handle_visibility(State.get_state("player_paused"))
	self.hide_all_menu()


func handle_visibility(new_is_visible: bool):
	if new_is_visible:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		self.show()
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		self.hide()

func hide_all_menu():
	for m in menu_container.get_children():
		m.hide()


func _on_state_player_paused(is_paused: bool):
	handle_visibility(is_paused)


func _on_ResumeButton_pressed():
	State.set_state("player_paused", false)


func _on_MainMenuButton_pressed():
	Util.change_level(main_menu_scene_path, true)
	Lobby.leave_lobby()


func _on_ExitButton_pressed():
	get_tree().quit()


func _on_OptionButton_pressed():
	self.hide_all_menu()
	option_menu.show()


func _on_AboutButton_pressed():
	self.hide_all_menu()
	about_menu.show()


func _on_multiplayer_button_pressed():
	self.hide_all_menu()
	multiplayer_menu.show()
