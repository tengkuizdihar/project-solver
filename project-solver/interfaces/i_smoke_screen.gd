extends Node
class_name ISmokeScreen

signal vision_smoked(intensity)  # intensity is float


func set_vision_intensity(intensity: float) -> void:
	emit_signal("vision_smoked", intensity)


func reset_vision_intensity() -> void:
	emit_signal("vision_smoked", 0.0)