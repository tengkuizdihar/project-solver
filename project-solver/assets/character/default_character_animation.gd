class_name DefaultCharacterAnimation
extends AnimationTree


func set_animation_state(character_velocity: Vector3) -> void:
    var velocity = Vector2()
    velocity.x = clampf(character_velocity.x, -13.0, 13.0) / 13.0
    velocity.y = -clampf(character_velocity.z, -13.0, 13.0) / 13.0

    self.set("parameters/Walk2D/BlendSpace2D/blend_position", velocity)