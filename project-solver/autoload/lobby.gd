extends Node

###################################################################################################
# CLIENT & SERVER
###################################################################################################

const MULTIPLAYER_COMPRESSION = ENetConnection.COMPRESS_ZLIB

func _ready():
	self.create_server_and_enter_lobby_offline()

	multiplayer.peer_connected.connect(_peer_connected)
	multiplayer.peer_disconnected.connect(_peer_disconnected)
	multiplayer.server_disconnected.connect(_server_disconnected)


func _process(_delta):
	if multiplayer.has_multiplayer_peer():
		multiplayer.poll()


func _peer_connected(peer_id: int) -> void:
	print(Global.LOG_INFO, "peer %d is connected, is_server: %s" % [peer_id, multiplayer.is_server()])

	var game_state = State.get_state("game_mode_state")
	if multiplayer.is_server():
		GameMode.gmd_change_state.rpc(game_state, "res://world_item/levels/multiplayer_test_level.tscn")


func _peer_disconnected(peer_id: int) -> void:
	print(Global.LOG_INFO, "peer %d is disconnected, is_server: %s" % [peer_id, multiplayer.is_server()])

	if multiplayer.is_server():
		var current_info = State.get_state("multiplayer_lobby_player_info").duplicate(true)
		current_info.erase(peer_id)

		State.set_state("multiplayer_lobby_player_info", current_info)
		self.set_lobby_information.rpc(current_info)
		GameMode.gmd_remove_player.rpc([peer_id])

	if not multiplayer.is_server():
		self.force_to_main_menu()  # happens when server is stopped


# TODO: for now server == lobby master, change this in the future.
func is_lobby_master() -> bool:
	return multiplayer.has_multiplayer_peer() and multiplayer.is_server()


# will return true if it's lobby master or is in single player mode
func is_authoritative() -> bool:
	return self.is_lobby_master() or not multiplayer.has_multiplayer_peer()


func leave_lobby() -> void:
	State.reset_excluded_state()

	if multiplayer.has_multiplayer_peer():
		if multiplayer.is_server():
			self.force_to_main_menu.rpc()

		multiplayer.multiplayer_peer = null
		self.create_server_and_enter_lobby_offline()


func kick_player_from_lobby(peer_id: int) -> void:
	if multiplayer.multiplayer_peer:
		multiplayer.multiplayer_peer.disconnect_peer(peer_id)


func is_peer_id_me(peer_id: int) -> bool:
	return peer_id == multiplayer.get_unique_id()


func lobby_master_change_level_path(level_path: String) -> void:
	var new_state = State.build_multiplayer_lobby_info(level_path)
	self.set_multiplayer_lobby_info.rpc(new_state)


###################################################################################################
# SERVER
###################################################################################################


func create_server_and_enter_lobby_offline() -> void:
	var uid = multiplayer.get_unique_id()
	State.set_state("multiplayer_lobby_password", "")
	State.set_state("multiplayer_lobby_player_info", {
		uid: State.build_multiplayer_lobby_player_info("player", 0, 0, 0, Global.TEAM.DEFAULT)
	})


# Create a server and enter the lobby immediately.
# Useful for self hosting on your own computer, but still want to play.
# For dedicated servers, you may use create_server instead.
func create_server_and_enter_lobby(nametag: String, password: String, port: int) -> void:
	self.create_server(port)

	var uid = multiplayer.get_unique_id()
	State.set_state("multiplayer_lobby_is_entered", true)
	State.set_state("multiplayer_lobby_password", password)
	State.set_state("multiplayer_lobby_player_info", {
		uid: State.build_multiplayer_lobby_player_info(nametag, 0, 0, 0, Global.TEAM.DEFAULT)
	})


func create_server(port: int) -> void:
	if multiplayer.has_multiplayer_peer():
		multiplayer.multiplayer_peer = null

	var peer = ENetMultiplayerPeer.new()
	var err = peer.create_server(port)
	if err != OK:
		Util.handle_err(err)
		return

	peer.host.compress(MULTIPLAYER_COMPRESSION)
	multiplayer.multiplayer_peer = peer
	get_tree().multiplayer_poll = false


# Enter a lobby, only used by clients.
# Self hosting server should instead use create_server_and_enter_lobby.
# BUG: password should be questioned by the server to the client instead of the other way around
#      flow:
#          1. client connect to server
#          2. server get the peer id with _peer_connected
#          3. still in _peer_connected, server rpc the client with interogate_password.rpc() that returns a string
#          4. still in _peer_connected, use the string and authorize it with the one we have
#          5. if correct, don't disconnect the client
@rpc("any_peer", "call_remote", "reliable")
func enter_lobby(nametag: String, password: String) -> void:
	if not multiplayer.is_server():
		return

	var uid = multiplayer.get_remote_sender_id()
	if State.get_state("multiplayer_lobby_password") != password:
		multiplayer.multiplayer_peer.disconnect_peer(uid)

	var current_info = State.get_state("multiplayer_lobby_player_info").duplicate(true)
	current_info[uid] = State.build_multiplayer_lobby_player_info(nametag, 0, 0, 0, Global.TEAM.DEFAULT)

	State.set_state("multiplayer_lobby_player_info", current_info)
	self.set_lobby_information.rpc(current_info)


###################################################################################################
# CLIENT
###################################################################################################

func connect_and_enter_lobby(nametag: String, ip_address: String, port: int, password: String) -> void:
	var err = connect_to_server(ip_address, port)
	if err != OK:
		# TODO: alert the user of connection errors by UI
		Util.handle_err(err)
		return

	await multiplayer.connected_to_server

	State.set_state("multiplayer_lobby_is_entered", true)
	enter_lobby.rpc_id(MultiplayerPeer.TARGET_PEER_SERVER, nametag, password)


# will return the error when connecting to server
func connect_to_server(ip_address: String, port: int) -> int:
	var err = 0

	if multiplayer.has_multiplayer_peer():
		multiplayer.multiplayer_peer = null

	var peer = ENetMultiplayerPeer.new()
	err = peer.create_client(ip_address, port)
	if err != OK:
		Util.handle_err(err)
		return err

	peer.host.compress(MULTIPLAYER_COMPRESSION)
	multiplayer.multiplayer_peer = peer
	get_tree().multiplayer_poll = false

	return err


@rpc("authority", "call_local", "reliable")
func set_multiplayer_lobby_info(multiplayer_lobby_info: Dictionary) -> void:
	State.set_state("multiplayer_lobby_info", multiplayer_lobby_info)


@rpc("authority", "call_remote", "reliable")
func set_lobby_information(server_lobby_info: Dictionary) -> void:
	if multiplayer.is_server():
		return

	State.set_state("multiplayer_lobby_player_info", server_lobby_info)


func server_sync_lobby_info_to_peers() -> void:
	var lobby_info = State.get_state("multiplayer_lobby_player_info")
	self.set_lobby_information.rpc(lobby_info)


# TODO: _damage is not yet synced, should be a mechanism to sync it after round ends (GMD shouldn't be bothered with damage done)
func set_lobby_info_stats(peer_id: int, kill: int, death: int, assist: int, _damage: float, team_enum: Global.TEAM) -> void:
	var current_info = State.get_state("multiplayer_lobby_player_info").duplicate(true)
	current_info[peer_id] = State.build_multiplayer_lobby_player_info(current_info[peer_id].nametag, kill, death, assist, team_enum)
	State.set_state("multiplayer_lobby_player_info", current_info)


func set_player_team(peer_id: int, team_enum: Global.TEAM) -> Dictionary:
	var info = State.get_state("multiplayer_lobby_player_info").duplicate(true)
	info[peer_id] = State.build_multiplayer_lobby_player_info(info[peer_id].nametag, info[peer_id].kill, info[peer_id].death, info[peer_id].assist, team_enum)
	State.set_state("multiplayer_lobby_player_info", info)
	return info


@rpc("authority", "call_local", "reliable")
func load_map(map_path: String, game_mode: int, game_mode_state = {}) -> void:
	Util.change_level(map_path, false, game_mode, game_mode_state)  # don't reset excluded because multiplayer state is already populated in lobby


@rpc("authority", "call_remote", "reliable")
func force_to_main_menu() -> void:
	Util.change_level("res://ui/main_menu.tscn", true)


func _server_disconnected() -> void:
	self.leave_lobby()
