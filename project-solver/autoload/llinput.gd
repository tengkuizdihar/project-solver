# LLInput.gd
# Low Latency Input is a custom node that will poll the input from input event
extends Node

const NO_DATA_VALUE = -1

const TIMEOUT_FRAMES = 1

# All of the input that will be registered
const CAPTURED_EVENTS = [
	"player_crouch",
	"player_walk",
	"player_weapon_swap",
	"player_weapon_gun_primary",
	"player_weapon_gun_secondary",
	"player_weapon_gun_knife",
	"player_weapon_drop",
	"player_interact",
	"player_forward",
	"player_backward",
	"player_right",
	"player_left",
	"player_reload",
	"player_shoot_primary",
	"player_shoot_secondary",
	"player_jump",
	"player_shoot_primary",
	"player_shoot_secondary",
]

# The format is
# "input_name" => frame
var inputs = {}

# this is a list of processes that will consume mouse_input
# "true" means it's been consumed
var mouse_input = {
	Global.PROCESS_CONSUMER.PROCESS: Vector2(),
	# Global.PROCESS_CONSUMER.PHYSICS_PROCESS: Vector2(),
}

const MOUSE_SENSITIVITY = 0.0008

func _ready():
	for c in CAPTURED_EVENTS:
		inputs["%s|%s" % [c, "pressed"]] = NO_DATA_VALUE
		inputs["%s|%s" % [c, "released"]] = NO_DATA_VALUE


func _input(event):
	for c in CAPTURED_EVENTS:
		if event.is_action_pressed(c):
			inputs["%s|%s" % [c, "pressed"]] = Engine.get_physics_frames()
		if event.is_action_released(c):
			inputs["%s|%s" % [c, "released"]] = Engine.get_physics_frames()

	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		var fov_ratio = Util.fov_from_zoom_mode(State.get_state("player_zoom_mode")) / Global.FOV_DEFAULT
		var speed = Config.state.player.mouse_speed * MOUSE_SENSITIVITY * fov_ratio

		for k in mouse_input.keys():
			mouse_input[k] += Vector2(event.relative.x * speed, event.relative.y * speed)


func consume_input(event_name: String) -> bool:
	var input_frame = inputs.get(event_name)

	if inputs.has(event_name):
		var current_physics_frame = Engine.get_physics_frames()
		var is_consumable = input_frame + TIMEOUT_FRAMES > current_physics_frame

		return is_consumable
	else:
		print_stack()
		printerr("Event name %s isn't registered in CAPTURED_EVENTS" % event_name)
		return false


func consume_mouse_input(process: int) -> Vector2:
	var temp = mouse_input[process]
	mouse_input[process] = Vector2()
	return temp


func get_player_actions() -> Array[int]:
	var actions: Array[int] = []
	if is_action_pressed("player_walk"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.WALK)

	if consume_input("player_jump|pressed"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.JUMP_JUST_FRAME)

	if is_action_pressed("player_jump"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.JUMP)

	if is_action_pressed("player_shoot_primary"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.SHOOT_1_PRESSED)

	if is_action_released("player_shoot_primary"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.SHOOT_1_RELEASED)

	if is_action_pressed("player_shoot_secondary"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.SHOOT_2_PRESSED)

	if is_action_released("player_shoot_secondary"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.SHOOT_2_RELEASED)

	if consume_input("player_weapon_drop|pressed"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.DROP_WEAPON)

	if is_action_pressed("player_crouch"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.CROUCH)

	if is_action_pressed("player_reload"):
		actions.append(Global.PLAYER_INPUT_ACTIONS.RELOAD)

	return actions


func is_action_pressed(event_name: String) -> bool:
	var input_key = "%s|%s" % [event_name, "pressed"]
	var input_value = inputs.get(input_key, NAN)

	if input_value != NAN:
		return Input.is_action_pressed(event_name) or consume_input(input_key)
	else:
		print_stack()
		printerr("Event name %s isn't registered in CAPTURED_EVENTS" % event_name)
		return false


func is_action_released(event_name: String) -> bool:
	var input_key = "%s|%s" % [event_name, "released"]
	var input_value = inputs.get(input_key, NAN)

	if input_value != NAN:
		return Input.is_action_just_released(event_name) or consume_input(input_key)
	else:
		print_stack()
		printerr("Event name %s isn't registered in CAPTURED_EVENTS" % event_name)
		return false
