extends Node

enum Mode {
	RANDOM_SINGLE = 1, # Abreviated to RS or rs
	SPRAY_SINGLE = 2, # Abreviated to SS or ss
	RANDOM_SINGLE_TIMED = 3, # Abreviated to RST or rst
}

signal score_changed(score)
signal mode_changed(mode)
signal rst_finished(elapsed_seconds, max_hit, shot_count_hit, shot_count_not_hit)

var mode = Mode.RANDOM_SINGLE_TIMED
var score: int = 0

var rst_physics_tick_start: int = 0
var rst_max_hit_before_timer: int = 3 # TODO: set to 25/50/75 checked release, hardcoded for now
var rst_shot_count_hit = 0
var rst_shot_count_not_hit = 0


func _ready():
	self.score_changed.connect(self._on_self_score_changed)
	PlayerStats.shot_fired.connect(self._on_PlayerStats_shot_fired)

	self.training_history = self.load_history()

	if self.training_history[RST_MAX_HIT_BEFORE_TIMER] > 0:
		self.rst_max_hit_before_timer = self.training_history[RST_MAX_HIT_BEFORE_TIMER]

	self.training_history_updated.connect(self._on_self_training_history_updated)
	self.emit_signal("training_history_updated", self.training_history.duplicate(true)) # this duplicate might be slow and unnecessary


func change_score(new_value: int) -> int:
	score = new_value
	emit_signal("score_changed", score)
	return new_value


func change_mode(mode_value) -> void:
	mode = mode_value
	emit_signal("mode_changed", mode)


func add_score() -> int:
	return change_score(score + 1)


func reset_score() -> int:
	return change_score(0)


func reset_state(new_mode = Mode.RANDOM_SINGLE_TIMED):
	var _new_score = reset_score()
	change_mode(new_mode)


func get_current_mode_name() -> String:
	return get_mode_name(mode)


func get_mode_name(value: int) -> String:
	match value:
		Mode.RANDOM_SINGLE:
			return "RANDOM"
		Mode.SPRAY_SINGLE:
			return "SPRAY"
		Mode.RANDOM_SINGLE_TIMED:
			return "RANDOM TIMED"
		_:
			printerr("Getting mode name based checked value that doesn't exist!")
			return ""


func _on_self_score_changed(new_score: int) -> void:
	match mode:
		Mode.RANDOM_SINGLE, Mode.SPRAY_SINGLE:
			pass
		Mode.RANDOM_SINGLE_TIMED:
			if new_score == 1:
				rst_physics_tick_start = Engine.get_physics_frames()
			elif new_score == rst_max_hit_before_timer:
				var current_tick = Engine.get_physics_frames()
				var elapsed_tick = current_tick - rst_physics_tick_start
				var second_per_tick = 1.0 / Engine.physics_ticks_per_second
				var elapsed_seconds = elapsed_tick * second_per_tick

				emit_signal("rst_finished", elapsed_seconds, rst_max_hit_before_timer, self.rst_shot_count_hit, self.rst_shot_count_not_hit)
		_:
			printerr("Getting mode name based checked value that doesn't exist!")


func reset_rst_state() -> void:
	self.rst_physics_tick_start = 0
	self.rst_shot_count_hit = 0
	self.rst_shot_count_not_hit = 0

	var _new_score = Score.reset_score()


func reset_rst_metronome_state() -> void:
	self.rst_physics_tick_start = 0
	self.rst_shot_count_hit = 0
	self.rst_shot_count_not_hit = 0

	var _new_score = Score.reset_score()


######################################
# IN-MEMORY TRAINING HISTORY
# Below are function for saving training history.
# It's decided to use in-memory instead of sqlite-like solution for simplicity of development.
# In the future, maybe we could use an online based solution.
######################################

signal training_history_updated(history_dict)

const HISTORY_FILE_LOCATION = "user://training_history.json"
const RST_HISTORY_KEY_V1 = "rst_v1" # for posterity, no need to track minor or patch version, just major version where there's a breaking changes
const RST_METRONOME_KEY_V1 = "rst_metronome_v1"
const RST_CURRENT_METRONOME_BPM = "rst_managed_bpm"
const RST_MAX_HIT_BEFORE_TIMER = "rst_max_hit_before_timer"
const RST_HISTORY_ENTRY_LIMIT_V1 = 50 # how much history we are trying to keep
const TRAINING_HISTORY_DEFAULT = {
	# list of dictionary in the form of:
	# {
	#   "create_datetime": "2022-12-24 15:38:19",   # when this training is recorded in format of "YYYY-MM-DD HH:MM:SS" checked UTC, for godot 3, this is generated with `Time.get_datetime_string_from_system(true, true)`
	#   "completion_time_second": 8.123,            # time until completion in second
	#   "accuracy": 1.0,                            # range of 0.0 to 1.0 inclusive, 0 means no shot hits the target, 1 means all shot is checked target
	#   "target_count": 25                          # number of target being hit until completion
	# }
	RST_HISTORY_KEY_V1: [],

	# list of dictionary in the form of:
	# {
	#   "create_datetime": "2022-12-24 15:38:19",   # when this training is recorded in format of "YYYY-MM-DD HH:MM:SS" checked UTC, for godot 3, this is generated with `Time.get_datetime_string_from_system(true, true)`
	#   "completion_time_second": 8.123,            # time until completion in second
	#   "accuracy": 1.0,                            # range of 0.0 to 1.0 inclusive, 0 means no shot hits the target, 1 means all shot is checked target
	#   "target_count": 25                          # number of target being hit until completion
	#   "bpm": 60                                   # bpm used for training
	# }
	RST_METRONOME_KEY_V1: [],
	RST_CURRENT_METRONOME_BPM: 60.0,
	RST_MAX_HIT_BEFORE_TIMER: 25
}

var training_history = TRAINING_HISTORY_DEFAULT.duplicate(true)

func rst_add_history_and_save(create_datetime: String, completion_time_second: float, accuracy: float, target_count: int) -> void:
	# TODO: optimize solution for removing n-oldest entry if above RST_HISTORY_ENTRY_LIMIT_V1
	if self.training_history[RST_HISTORY_KEY_V1].size() >= RST_HISTORY_ENTRY_LIMIT_V1:
		var count_to_be_deleted = self.training_history[RST_HISTORY_KEY_V1].size() - RST_HISTORY_ENTRY_LIMIT_V1 + 1
		for _i in range(count_to_be_deleted):
			self.training_history[RST_HISTORY_KEY_V1].remove_at(0)

	self.training_history[RST_HISTORY_KEY_V1].append({
		"create_datetime": create_datetime,
		"completion_time_second": completion_time_second,
		"accuracy": accuracy,
		"target_count": target_count
	})

	self.save_history(self.training_history)
	self.emit_signal("training_history_updated", self.training_history.duplicate(true)) # this duplicate might be slow and unnecessary


func rst_metronome_add_history_and_save(create_datetime: String, completion_time_second: float, accuracy: float, target_count: int, bpm: float) -> void:
	# TODO: optimize solution for removing n-oldest entry if above RST_HISTORY_ENTRY_LIMIT_V1
	if self.training_history[RST_METRONOME_KEY_V1].size() >= RST_HISTORY_ENTRY_LIMIT_V1:
		var count_to_be_deleted = self.training_history[RST_METRONOME_KEY_V1].size() - RST_HISTORY_ENTRY_LIMIT_V1 + 1
		for _i in range(count_to_be_deleted):
			self.training_history[RST_METRONOME_KEY_V1].remove_at(0)

	self.training_history[RST_METRONOME_KEY_V1].append({
		"create_datetime": create_datetime,
		"completion_time_second": completion_time_second,
		"accuracy": accuracy,
		"target_count": target_count,
		"bpm": bpm
	})

	self.save_history(self.training_history)
	self.emit_signal("training_history_updated", self.training_history.duplicate(true)) # this duplicate might be slow and unnecessary


func rst_metronome_change_bpm(bpm: float) -> void:
	self.training_history[RST_CURRENT_METRONOME_BPM] = bpm
	self.save_history(self.training_history)


func rst_change_max_hit(max_hit: int) -> void:
	self.training_history[RST_MAX_HIT_BEFORE_TIMER] = max_hit
	self.save_history(self.training_history)


func save_history(new_history: Dictionary) -> void:
	var history_file = FileAccess.open(HISTORY_FILE_LOCATION, FileAccess.WRITE)
	history_file.store_string(JSON.stringify(new_history))


func emit_history_changes() -> void:
	self.emit_signal("training_history_updated", self.training_history.duplicate(true))


func load_history() -> Dictionary:
	var loaded_dictionary = TRAINING_HISTORY_DEFAULT.duplicate(true)

	if FileAccess.file_exists(HISTORY_FILE_LOCATION):
		var history_file = FileAccess.open(HISTORY_FILE_LOCATION, FileAccess.READ)
		var content = history_file.get_as_text()

		# TODO: add more validation json structure and things like that.
		if content != "": # why this function is being stupidly quirky, seriously, why the hell "valid" equals to empty string?
			var parsed_dictionary = JSON.parse_string(content)
			if parsed_dictionary:
				loaded_dictionary = parsed_dictionary

	return loaded_dictionary


# apply some training specific settings
func _on_self_training_history_updated(new_training_history: Dictionary) -> void:
	if new_training_history[RST_MAX_HIT_BEFORE_TIMER] > 0:
		self.rst_max_hit_before_timer = new_training_history[RST_MAX_HIT_BEFORE_TIMER]


func _on_PlayerStats_shot_fired(_player: Node3D, hit_list: Array) -> void:
	if rst_physics_tick_start > 0:
		var has_target = false
		for hit in hit_list:
			if "is_training_target" in hit:
				has_target = true
				break

		if has_target:
			rst_shot_count_hit += 1
		if not has_target:
			rst_shot_count_not_hit += 1
