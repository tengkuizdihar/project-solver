extends Label

func _ready():
	self.text = State.get_state("player_center_text")
	Util.handle_err(State.connect("state_player_center_text",Callable(self,"_on_state_player_center_text")))


func _on_state_player_center_text(new: String) -> void:
	self.text = new
