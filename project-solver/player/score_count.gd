extends Label

func _ready():
	if not (State.get_state("game_mode") == Global.GAME_MODE.AIM_TRAINING and Score.mode == Score.Mode.RANDOM_SINGLE_TIMED):
		hide()

	Util.handle_err(Score.connect("score_changed", Callable(self, "_on_score_changed")))
	self.text = "0"

func _on_score_changed(new_score: int) -> void:
	self.text = "%d" % new_score