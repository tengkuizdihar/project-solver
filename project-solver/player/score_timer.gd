extends Label

func _ready():
	var aim_training = State.get_state("game_mode") == Global.GAME_MODE.AIM_TRAINING and Score.mode == Score.Mode.RANDOM_SINGLE_TIMED
	var default_mode = State.get_state("game_mode") == Global.GAME_MODE.DEFAULT

	if not (aim_training or default_mode):
		hide()


func _physics_process(_delta):
	var mode = State.get_state("game_mode")
	var state = State.get_state("game_mode_state")
	match mode:
		Global.GAME_MODE.AIM_TRAINING:
			if Score.rst_physics_tick_start > 0:
				var current_tick = Engine.get_physics_frames()
				var elapsed_tick = current_tick - Score.rst_physics_tick_start
				var second_per_tick = 1.0 / Engine.physics_ticks_per_second
				var elapsed_seconds = elapsed_tick * second_per_tick

				var minute = floor(elapsed_seconds / 60)
				self.text = "%02d:%02.3f" % [minute, fmod(elapsed_seconds, 60.0)]
			else:
				self.text = "FINISHED"
		Global.GAME_MODE.DEFAULT:
			# counting down from 10 minutes for example to 10
			var current_time = Time.get_unix_time_from_system()
			var time_left = max(state.gmd.state_ongoing_end_unix - current_time, 0.0)
			var minute_part = int(floor(time_left / 60.0))
			var second_part = fmod(time_left, 60.0)

			var time_text = "%d:%.1f" % [minute_part, second_part]

			if self.text != time_text:
				self.text = time_text
