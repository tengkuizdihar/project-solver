extends CharacterBody3D

var is_training_target = true  # HACK: used for marker, if you're making this from scratch, always think about this.

@onready var i_interact = $IInteract

func activate():
	self.show()
	self.collision_layer = Global.PHYSICS_LAYERS.WORLD
	self.collision_mask = Global.PHYSICS_LAYERS.WORLD


func deactivate():
	self.hide()
	self.collision_layer = 0
	self.collision_mask = 0
