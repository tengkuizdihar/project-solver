extends Node3D

# You could optimize this, but it make things easier to program this way.
# As Carlos Sainz said, "Stop Inventing, Stop Inventing." Lmao.
var target_list = [] # list of node reference
var alive_target = {} # node reference => null
var dead_target = {} # node reference => null

var intended_target_alive_count = 0 # should be 20% of all available target count

@onready var targets_node = $Targets
@onready var middle_target = $Targets/TrainingTarget

func _ready():
	var all_target = self.targets_node.get_children()
	for i in all_target:
		self.target_list.append(i)
		i.i_interact.connect("interacted",Callable(self,"_on_target_hit").bind(i))

	intended_target_alive_count = int(ceil(all_target.size() * 0.2))

	self.deactivate_all()
	self.activate_target(middle_target)


func deactivate_all() -> void:
	self.alive_target.clear()
	for i in self.target_list:
		i.deactivate()
		self.dead_target[i] = null


func activate_target(target: Node3D) -> void:
	target.activate()
	self.alive_target[target] = null
	self.dead_target.erase(target)


func deactivate_target(target: Node3D) -> void:
	target.deactivate()
	self.dead_target[target] = null
	self.alive_target.erase(target)


func _on_target_hit(_interactor, target_node: Node) -> void:
	var new_score = Score.add_score()
	if new_score < Score.rst_max_hit_before_timer:
		if alive_target.size() < intended_target_alive_count: # I KNOW I KNOW NESTED IF BAD YES GET OFF MY TAIL BRO ITS LATE AND I WANT TO SLEEP
			var will_activate_count = intended_target_alive_count - alive_target.size()
			var random_target_list = dead_target.keys()
			random_target_list.shuffle()
			var iteration_count = min(will_activate_count, random_target_list.size())

			for i in range(iteration_count):
				var random_target = random_target_list[i]
				self.activate_target(random_target)

		# deactivate later so it will not be spawned again immediately after being killed
		# maybe there will be a bug here somewhere, maybe when dead_target.size() == 0?
		self.deactivate_target(target_node)
	else:
		self.deactivate_all()
		self.activate_target(middle_target)
