extends Camera3D

@onready var team_menu_ui = $TeamMenu
@onready var starting_menu_ui = $StartingMenu
@onready var ending_menu_ui = $EndingMenu

#############################
# BUILT IN AND SIGNAL
#############################

func _ready() -> void:
	Util.handle_err(State.connect("state_game_mode_state", self._on_game_mode_state))
	gmd_ui_setter(State.get_state("game_mode_state"))


func _physics_process(_delta):
	if not self.is_current() and is_all_visible():
		hide_all_ui()


func _on_game_mode_state(state):
	var game_mode = State.get_state("game_mode")
	match game_mode:
		Global.GAME_MODE.DEFAULT:
			gmd_ui_setter(state)

#############################
# REGULAR FUNCTIONS
#############################

func is_all_visible() -> bool:
	var all_visible = team_menu_ui.is_visible() or \
	starting_menu_ui.is_visible() or \
	ending_menu_ui.is_visible()

	return all_visible


func hide_all_ui() -> void:
	team_menu_ui.handle_visibility(false)
	starting_menu_ui.hide()
	ending_menu_ui.hide()


func gmd_ui_setter(state: Dictionary) -> void:
	match state.state:
		Global.DEFAULT_GAME_MODE_STATE.STARTING:
			hide_all_ui()
			starting_menu_ui.show()
		Global.DEFAULT_GAME_MODE_STATE.ONGOING:
			hide_all_ui()
			team_menu_ui.handle_visibility(true)
		Global.DEFAULT_GAME_MODE_STATE.ENDING:
			hide_all_ui()
			ending_menu_ui.show()