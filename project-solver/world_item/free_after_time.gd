@tool
extends GPUParticles3D

@export var free_timer_path: NodePath

var timer: Timer = null

func _get_configuration_warnings() -> PackedStringArray:
	var test = get_node(free_timer_path)
	if not (test and test is Timer):
		return ["free_timer_path is not pointing at a timer"]
	return []


func _ready() -> void:
	if not Engine.is_editor_hint():
		emitting = true
		timer = get_node(free_timer_path)
		if timer.connect("timeout",Callable(self,"queue_free")):
			printerr("Failed connecting timer::timeout to self::queue_free")
