extends Node3D

var player_scene = preload("res://player/player.tscn")
var player_weapon_no_recoil_infinite = preload("res://world_item/guns/infinite_semi_auto_pistol.tscn")

func _ready():
	# Resurrect Them
	var universal_spawn_point = get_tree().get_nodes_in_group(Global.SPAWN_TYPE_GROUP_UNIVERSAL).pop_front()
	var new_player = player_scene.instantiate()
	new_player.global_transform = universal_spawn_point.global_transform

	# Add To World3D
	# Consequently, will be handled by handle_added_player automagically in _on_tree_node_added
	Util.add_to_world(new_player)

	# give player gun with infinite ammo and no recoil
	var aiming_weapon = player_weapon_no_recoil_infinite.instantiate()
	Util.add_to_world(aiming_weapon)
	new_player.replace_weapon_and_free_existing(aiming_weapon)
	new_player._switch_weapon_routine(aiming_weapon.weapon_slot)

	# disable player movement input (aiming is still enabled) and processing
	new_player.disable_movement = true

	# disable player throwing guns (using "g")
	new_player.disable_weapon_drop = true
	
	Score.rst_finished.connect(self._on_self_rst_finished)


func _on_self_rst_finished(elapsed_seconds: float, max_hit_before_timer: int, shot_count_hit: int, shot_count_not_hit: int) -> void:
	var accuracy = 0.0
	if (shot_count_hit + shot_count_not_hit) > 0:
		accuracy = float(shot_count_hit) / float(shot_count_hit + shot_count_not_hit)

	Score.rst_add_history_and_save(Time.get_datetime_string_from_system(true, true), elapsed_seconds, accuracy, max_hit_before_timer)
	Score.reset_rst_state()
