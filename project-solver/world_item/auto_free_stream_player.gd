extends AudioStreamPlayer3D
class_name AutoFreeStreamPlayer

func custom_play(from_position: float = 0.0) -> void:
	play(from_position)
	await get_tree().create_timer(self.stream.get_length()).timeout
	queue_free()
