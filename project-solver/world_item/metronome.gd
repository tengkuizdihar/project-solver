extends Node

signal ticked(bpm, tick_count)  # bpm is in float, tick_count in int

@export var play_metronome_sound = true
@export var is_enabled = false
@export var bpm = 100.0

var tick_count = 0
var _metronome_player
var _time_passed = 0.0

func _ready():
	Util.handle_err(self.connect("ticked", Callable(self, "_on_metronome_ticked")))
	_metronome_player = get_node("%MetronomePlayer")


func _physics_process(delta):
	if is_enabled:
		var is_first_tick = tick_count == 0
		if is_first_tick:
			tick_count += 1
			self.emit_signal("ticked", self.bpm, self.tick_count)

		self._time_passed += delta
		if not is_first_tick and self._time_passed >= (60.0 / self.bpm):
			tick_count += 1
			self._time_passed = 0.0
			self.emit_signal("ticked", self.bpm, self.tick_count)


func _on_metronome_ticked(_bpm: float, _tick_count: int) -> void:
	if play_metronome_sound:
		_metronome_player.play()


func start_metronome() -> void:
	_time_passed = 0.0
	tick_count = 0
	is_enabled = true


func stop_metronome() -> void:
	_time_passed = 0.0
	tick_count = 0
	is_enabled = false
