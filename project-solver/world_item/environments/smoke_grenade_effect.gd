extends Node3D

@export var duration_second = 20.0
@export var smoke_transition_eased = 0.0  # (float, EASE)

var smoke_volume = null
var is_shrinking = false
var _original_scale = Vector3.ZERO
var _expansion_rate = Vector3.ONE * 10

func _ready():
	smoke_volume = $SmokeVolume

	_original_scale = smoke_volume.scale
	smoke_volume.scale = Vector3.ZERO

	var timer = get_tree().create_timer(duration_second)
	timer.connect("timeout", Callable(self, "_on_timer_timeout"))


func _physics_process(delta):
	if smoke_volume.scale.length() < _original_scale.length() and not is_shrinking:
		smoke_volume.scale = smoke_volume.scale + (delta * _expansion_rate)

	if smoke_volume.scale.length() >= 0 and is_shrinking:
		smoke_volume.scale = smoke_volume.scale - (delta * _expansion_rate)

	if smoke_volume.scale.length() >= _original_scale.length():
		smoke_volume.scale = _original_scale

	if smoke_volume.scale.length() <= 0.2 and is_shrinking:
		queue_free()

func _on_timer_timeout():
	is_shrinking = true
