---
name: Squashing Todos
about: Issue to track mentioned todos in the code or documentation as a way to get rid of technical debts
---

## Please Confirm The Following (required):
- [x] Check the boxes as such.
- [ ] I have searched the [issues](https://codeberg.org/solver-orgz/project-solver/issues), and I didn't find a solution to my problem / an answer to my question.
- [ ] If you upload an image or other content, please make sure you have read and understood the [Codeberg Terms of Use](https://codeberg.org/Codeberg/org/src/branch/main/TermsOfUse.md)

## Where Did You Get Project Solver from (required - choose one):
- [ ] I built it myself from source code (specify tag / commit)
- [ ] From a thirdparty that's not affiliated with us

## Your Project Solver Version Is (required):

(This can be found in our Main Menu)

## Your Related Todo Is (required):

the permalink to the todo: ...

```gdscript
# TODO: ....
```

</details>

## Your Platform Is (optional):

Not submitted.

<!--
- Operating System: eg. Ubuntu 20.04 LTS
- GPU: eg. Nvidia RTX 2070
- CPU: eg. Intel Core i7-13700K
-->