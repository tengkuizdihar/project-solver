---
name: Bug Report
about: Create a report to help us improve
---

## Please Confirm The Following (required):
- [x] Check the boxes as such.
- [ ] I have searched the [issues](https://codeberg.org/solver-orgz/project-solver/issues), and I didn't find a solution to my problem / an answer to my question.
- [ ] If you upload an image or other content, please make sure you have read and understood the [Codeberg Terms of Use](https://codeberg.org/Codeberg/org/src/branch/main/TermsOfUse.md)

## Where Did You Get Project Solver from (required - choose one):
* [ ] I built it myself from source code (specify tag / commit)
* [ ] From a thirdparty that's not affiliated with us

## Your Project Solver Version Is (required):

(This can be found in our Main Menu)

## Your Issue Is (required):

<!-- Please describe the behavior of Project Solver that you think is a bug -->

<details>
  <summary>Click to see my log under this fold</summary>

```
Here go lines of your log.
```
</details>

## Your Platform Is (required):

- Operating System: eg. Ubuntu 20.04 LTS
- GPU: eg. Nvidia RTX 2070
- CPU: eg. Intel Core i7-13700K